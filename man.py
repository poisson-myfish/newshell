from database import execs, commandHelp
from messages import Errors 

def getHelp(keyword):
    try:
        command = keyword[1]
    except:
        print('Invalid syntax')
    if command in execs:
        try:
            print(commandHelp[command])
        except:
            print('No manual entry for your command')
    else:
        print(Errors.no_command)
