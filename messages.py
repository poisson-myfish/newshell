from database import ShellData
datafile = open('datafile.txt', 'r').read().split()

if datafile[1] == 'win':
    os = 'Windows'
elif datafile[1] == 'linux':
    os = 'Linux'
elif datafile[1] == 'mac':
    os = 'macOS'
else:
    print('Datafile corrupted')

class Messages:
    install_default = '''
"Install" version 1.0
Used to install appbox files  
'''
    shell_info = f'NewShell version {ShellData.version}. Running on {os}'

class Errors:
    no_command = "Are you sure this is the correct command? I couldn't find it in my database."
