# Made by poisson
# Main file

import os
from messages import Messages, Errors
from functions import shell_info, list_execs, elevate
from install import install
from database import ShellData, Commands
from man import getHelp

os.system(Commands.clearcomm)
shell_info()

while True:
    keyword = input('%: ').split()
    if keyword[0] == 'show':
        try:
            string = keyword
            del string[0]
            print(''.join(string))
        except:
            print('Invalid syntax')
    elif keyword[0] == 'exit':
        exit()
    elif keyword[0] == 'wipe':
        os.system(Commands.clearcomm)
    elif keyword[0] == 'install':
        install(keyword)
    elif keyword[0] == 'shell-info':
        shell_info()
    elif keyword[0] == 'help':
        list_execs()
    elif keyword[0] == 'elevate':
        elevate(keyword)
    elif keyword[0] == 'man':
        getHelp(keyword)
    else:
        print(Errors.no_command)
