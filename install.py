def show_version():
    print('Install version 1.0')

def appbox(keyword):
    try:
        print('Installed appbox ' + keyword[2])
    except:
        print('Invalid syntax')

def install(keyword):
    try:
        if keyword[1] == 'help':
            show_version()
        if keyword[1] == 'appbox':
            appbox(keyword)
    except:
        print('Invalid syntax')