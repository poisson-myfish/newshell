datafile = open('datafile.txt', 'r').read().split()

execs = [
    'show',
    'wipe',
    'exit',
    'install',
    'shell-info',
    'elevate',
    'man'
]

class ShellData:
    version = 1.0

class Commands:
    clearcomm = datafile[0]

commandHelp = {
        'show': '''Helps the user print a string of text
USAGE: show [string]
EXAMPLE: show HelloWorld!

ARGUMENTS:
string: expects any string
        ''',

        'wipe': '''Wipes the screen
USAGE: wipe
EXAMPLE: wipe
''',

        'exit': '''Exits NewShell
USAGE: exit
EXAMPLE: exit
''',

        'install': '''Installs packages
USAGE: install [packageType] [packageName]
EXAMPLE: install appbox myApp

ARGUMENTS:
packageType: expects the package type to be installed
             possible values: appbox

packageName: expects the name of the package to be installed

WARNING: Currently in alpha stage, not installing any package
''',

        'shell-info': '''Shows current NewShell version and configuration
USAGE: shell-info
EXAMPLE: shell-info
''',

        'elevate': '''Elevates NewShell at a specific level
USAGE: elevate [level]
EXAMPLE: elevate systemshell

ARGUMENTS:
level: the level of elevation
       possible values: systemshell 
''',

        'man': '''Explains how a specific command is used
USAGE: man [command]
EXAMPLE: man show

ARGUMENTS:
command: expects a command name
'''
     }
