import os

os.system('cls')

print('''
 ____     ___ __    __  _____ __ __    ___  _      _     
 |    \   /  _]  |__|  |/ ___/|  |  |  /  _]| |    | |    
 |  _  | /  [_|  |  |  (   \_ |  |  | /  [_ | |    | |    
 |  |  ||    _]  |  |  |\__  ||  _  ||    _]| |___ | |___ 
 |  |  ||   [_|  `  '  |/  \ ||  |  ||   [_ |     ||     |
 |  |  ||     |\      / \    ||  |  ||     ||     ||     |
 |__|__||_____| \_/\_/   \___||__|__||_____||_____||_____|
                                                          
''')

print('Newshell first launch configuration')

keepinloop = True

while keepinloop == True:
    os = input('What OS are you using?[win, linux, mac]: ')
    if os == 'win':
        clearcomm = 'cls'
        keepinloop = False
    elif os == 'linux' or os == 'mac':
        clearcomm = 'clear'
        keepinloop = False
    else:
        print('Invalid entry')
keepinloop = True

datafile = open('datafile.txt', 'w+')
datafile.write(f'{clearcomm} {os}')

print('NewShell configured successfully. You can run it by running main.py from the root directory of your installation. For easier usage, you can add it to your environment variables')
