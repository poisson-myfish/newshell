# NewShell
A cross-platform, easy to use and open source shell written in Python


## How to install and configure
1. Clone the repository, or download it as a ZIP file
2. Run firstlaunch.py, and follow the instructions
3. Run main.py to use the shell
4. OPTIONAL: Add NewShell to the environment variables to be able to launch it from anywhere on the system

## Current commands:
1. ```show```
2. ```wipe```
3. ```exit```
4. ```install```
5. ```shellinfo```
6. ```elevate```
7. ```man```

## Datafile
The datafile, located at the root of your NewShell installation, by the name of ```datafile.txt``` is a file used to store information such as your OS for NewShell to work properly. Currently, the skeleton of the datafile is:

	clearcomm os


The ```clearcomm``` keyword refers to the command used to clear your screen. For example, on Windows it's ```cls``` and for *NIX-like systems it's ```clear```. The ```os``` keyword refers to the Operating System you are using.

The datafile will be configured using the ```firstlaunch.py``` script, located at the root of your NewShell installation. Running this script is recommended afer every update to make sure there are no errors.

**Made by poisson**
