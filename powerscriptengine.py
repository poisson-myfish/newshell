# This is the scripting engine. Here we will put the logic for interpreting scripts

def launchscript(keyword):
    try:
        path = keyword[1]
        execscript(path)
    except:
        print('Invalid syntax')

def execscript(path):
    fileread = open(path, 'r')
    print(fileread)
