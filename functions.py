from messages import Messages
from database import execs
import os

elevated = False

def shell_info():
    print(Messages.shell_info)

def elevate(keyword):
    try:
        if keyword[1] == 'systemshell':
            elevated = True
            while elevated == True:
                command = input('elevated<systemshell> %: ')
                if command == 'exit':
                    elevated = False
                else:
                    os.system(command)
        else:
            print('Elevation level not found')
    except:
        print('Invalid syntax')

def list_execs():
    for command in execs:
        print(command)
